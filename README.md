# Desafio Timeline Android

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!

Neste desafio queremos entender um pouco mais do seu conhecimento em desenvolvimento Android. Método de avaliação é simples, tendo seu código em mãos iremos validar a solução que você desenvolveu para o desafio

# Requisitos

Android Studio 3.4 ou superior

Utilizar Kotlin como linguagem

# Desafio

- Nível 1

> Desenvolver um simples aplicativo que comunica-se com a internet e proporcione uma experiência responsiva. Neste nível, você irá:
>   * Realizar um fetch de dados utilizando uma API
>   * Utilizar adapters e custom list para a exibição dos dados

    Os dados deverão ser exibidos em uma RecycleView, onde cada item da lista deverá conter
        - Origem 
        - Valor
    
    Apresentar os dados de lançamentos da API (https://gitlab.com/desafioandroid/api-desafio) - GET /lancamentos

> Conteúdo de suporte
>   * RECYCLEVIEW (https://developer.android.com/guide/topics/ui/layout/recyclerview) 
>   * RETROFIT (https://square.github.io/retrofit/)

*( exemplo conceitual https://dribbble.com/shots/3179815-Mobile-Banking-App)*

- Nível 2

> A partir do que foi desenvolvido no nível anterior. Neste projeto, você irá:
>   * Detalhar os lançamentos em uma nova tela
>   * Agrupar os lançamentos por mês
>   * Exibir o balanço de gastos para cada mês

    Na tela de detalhes do lançamento deverá ser exibido as informações:
        - Origem
        - Nome da categoria
        - Valor
        
    Utilizar a API de categorias para recuperar o nome da categoria (https://gitlab.com/desafioandroid/api-desafio) - GET /categorias

> Conteúdo de suporte
>   * INTENT (https://developer.android.com/reference/android/content/Intent)
>   * BUNDLE (https://developer.android.com/reference/android/os/Bundle)
>   * PARCELABLE (https://developer.android.com/reference/android/os/Parcelable)
>   * CONSTRAINTLAYOUT (https://developer.android.com/reference/android/support/constraint/ConstraintLayout)

- Nível 3

> Neste nível iremos implementar conceitos de arquitetura, com o objetivo de deixar a aplicação mais escalável. A partir do que foi desenvolvido nos níveis anteriores, neste projeto você irá:
>   * Implementar o padrão de projeto MVVM
    
    Utilizar as bibliotecas do Android Architecture Components
    
    Realizar testes unitários de classes lógicas
    
    Realizar testes de UI
    
> Conteúdo de suporte
>   * VIEWMODEL (https://developer.android.com/topic/libraries/architecture/viewmodel)
>   * LIVEDATA (https://developer.android.com/topic/libraries/architecture/livedata)
>   * ESPRESSO (https://developer.android.com/training/testing/espresso)


**Bom Desempenho!**

# QueremosVoceNoItau
